import { render, screen } from '@testing-library/react'
import App from './App'

// eslint standard update lagging behind the test, expect updates from node 18
/* eslint-disable no-undef */
test('renders learn react link', () => {
  render(<App />)
  const linkElement = screen.getByText(/learn react/i)
  expect(linkElement).toBeInTheDocument()
})
/* eslint-enable */

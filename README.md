# homepage-app

## homepage (front end)

### Development Server

The development server can be started via:
- Installing docker and running `cd homepage && ./bin/development-server`
- Installing node, npm and running `cd homepage && npm start`

### Pipeline Stages

#### Code Quality

In the dev requirements of package.json, eslint provides "Standard" formatted linting via the configuration file at `homepage/.eslintrc.json`

`npm run lint`

#### Testing

A node container will run the tests directly from `homepage/package.json`.

`npm run test`

### (History) Initialization

The React front end was initialized with the Meta-supplied create-react-app script. No ejection is initially planned.

`docker run --rm -v $PWD:/app -w /app -it --user node node:18 npx create-react-app homepage`
